package com.rambris;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class JacksonTests {

    @Test
    public void updateWithoutJsonTypeInfo() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String newMake="toyota";

        VehicleWihtoutJsonTypeInfo vehicle=new VehicleWihtoutJsonTypeInfo();
        vehicle.setMake("ford");
        vehicle = mapper.readerForUpdating(vehicle).readValue("{\"make\": \"" + newMake + "\"}");
        Assert.assertEquals("Make should have changed", newMake, vehicle.getMake());
    }

    @Test
    public void createNewInstance() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String make="toyota";
        String model="corolla";
        boolean leaded=false;
        String json="{\"make\": \""+make+"\", \"model\": \""+model+"\", \"fuel\":{\"leaded\": "+Boolean.toString(leaded)+"} }";
        VehicleWithJsonTypeInfo vehicle = mapper.readValue(json, VehicleWithJsonTypeInfo.class);
        Assert.assertEquals("Make is set", make, vehicle.getMake());
        Assert.assertEquals("Model is set", model, vehicle.getModel());
        Assert.assertNotNull("Fuel is set", vehicle.getFuel());
        Assert.assertEquals("Fuel is correct type", Petrol.class, vehicle.getFuel().getClass());
        Assert.assertEquals("Fuel leaded is the same", leaded, ((Petrol)vehicle.getFuel()).isLeaded());
        System.out.println(vehicle);

    }

    @Test
    public void updateWithJsonTypeInfo() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String newMake="toyota";

        VehicleWithJsonTypeInfo vehicle=new VehicleWithJsonTypeInfo();
        vehicle.setMake("ford");
        vehicle = mapper.readerForUpdating(vehicle).readValue("{\"make\": \"" + newMake + "\"}");
        Assert.assertEquals("Make should have changed", newMake, vehicle.getMake());
    }

}
