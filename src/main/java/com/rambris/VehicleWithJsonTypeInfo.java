package com.rambris;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


public class VehicleWithJsonTypeInfo {
    private String make;
    private String model;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "make")
    @JsonSubTypes({
            @JsonSubTypes.Type(value = Diesel.class, name = "mercedes"),
            @JsonSubTypes.Type(value = Petrol.class, name = "toyota")
    })
    private Fuel fuel;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    @Override
    public String toString() {
        return "WorkingVehicle{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", fuel=" + fuel +
                '}';
    }
}
