package com.rambris;

public class Petrol implements Fuel {
    private boolean leaded;

    public boolean isLeaded() {
        return leaded;
    }

    public void setLeaded(boolean leaded) {
        this.leaded = leaded;
    }

    @Override
    public String toString() {
        return "Petrol{" +
                "leaded=" + leaded +
                '}';
    }
}
