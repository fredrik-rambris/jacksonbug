package com.rambris;

public class VehicleWihtoutJsonTypeInfo {
    private String make;
    private String model;

    private Fuel fuel;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    @Override
    public String toString() {
        return "WorkingVehicle{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", fuel=" + fuel +
                '}';
    }
}
